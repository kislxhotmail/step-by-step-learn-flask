# Step by step learn Flask
## Getting started
### Preparation (Mac)
- [ ] Install python
    - [Downloads page](https://www.python.org/downloads/macos/)
- [ ] Install pip
    - [How to install Pip on Mac](https://phoenixnap.com/kb/install-pip-mac)
## Roadmap
- Virtual environment creation
    - venv
    - virtualenv
    - pipenv
- Restarting venv environment
- Install plugin on virtual environment
    - Based on Pipfile (_Provide default pipfile_)
    - Based on pip install
    - How to import and export plugin setting
- Run Flask
    - create file
        - app.py
            - Import module (_Provide default template_)
            - Routing setting
                1. Http methods - [GET, POST]
                2. Dynamic path
            - How to get frontend data
                1. GET
                2. POST
            - Return data
                1. str and number and dict
                2. json
                3. others - custom response ( include http status code )
                4. others - page (template)
                5. others - redirect to another routing
            - Run web service 
                1. by Flask Run
                    - install python-dotenv, then define detail on .flaskenv
                    - execute the command such as Set FLASK_APP = python file ..
                2. Python app.py
                    - define on app.py
                3. certificate setting
        - config.py
            - class Config
        - models.py ( will explain on DB part )
    - create folder
        - static
        - templates
- Connect DB
    - models.py
        - class define (db.Model)
        - [flask-sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)
    - config.py
        - Database host
        - Security
            - username & password
        - Multiple  DB bind
    - How to import class import python file
    - How to query / write / delete / adjust db by routing



